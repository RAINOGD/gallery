<?php
/**
 * Created by PhpStorm.
 * User: rain
 * Date: 8/12/17
 * Time: 1:10 PM
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Commentary;
use AppBundle\Form\CommentaryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PhotoController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     * @param int $photo_id
     * @Route("/photo/{photo_id}")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function photoAction(int $photo_id){

        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($photo_id);

        $commentaryForm = $this->createForm(CommentaryType::class, null, [
            'action' => $this->generateUrl('app_photo_newcomment', ['photo_id' => $photo_id]),
            'method' => 'POST'
        ]);

        $commentaries = $this->getDoctrine()
            ->getRepository('AppBundle:Commentary')
            ->findBy(['photo' => $photo_id]);

        $photoScores = [];
        $avgScore = null;

        foreach ($commentaries as $commentary){
            $photoScores[] = $commentary->getScore();
        }

        if (count($photoScores) > 0) {
            $avgScore = array_sum($photoScores) / count($photoScores);
        }

        return $this->render('@App/photo_detail.html.twig', [
            'photo' => $photo,
            'commForm' => $commentaryForm->createView(),
            'commentaries' => $commentaries,
            'avg' => $avgScore
        ]);
    }

    /**
     * @Method("POST")
     * @Route("/commentary/{photo_id}")
     * @param Request $request
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newCommentAction(Request $request, int $photo_id)
    {
        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($photo_id);

        $commentary = new Commentary();

        $commForm = $this->createForm(CommentaryType::class, $commentary);

        $commForm->handleRequest($request);

        if ($commForm->isValid() && $commForm->isSubmitted()){
            $commentary->setAuthor($this->getUser());
            $commentary->setPhoto($photo);

            $em = $this->getDoctrine()->getManager();
            $em->persist($commentary);
            $em->flush();
        }

        return $this->redirectToRoute('app_photo_photo', array(
            'photo_id' => $photo_id
        ));
    }
}