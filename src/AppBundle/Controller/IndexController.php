<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Form\CommentaryType;
use AppBundle\Form\FollowType;
use AppBundle\Form\LikeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class indexController
 * @package AppBundle\Controller
 */
class IndexController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/")
     */
    public function photosListAction(){
        $user = $this->getUser();
        if (!$user){
            $this->redirectToRoute('fos_user_security_login');
        }

        $photos = $this->getDoctrine()->getRepository('AppBundle:Photo')
            ->findAll();

        return $this->render('@App/photo_list.html.twig', [
            'photos' => $photos
        ]);
    }
}