<?php
/**
 * Created by PhpStorm.
 * User: rain
 * Date: 8/12/17
 * Time: 11:47 AM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Photo;
use AppBundle\Form\addPhotoType;
use AppBundle\Form\deletePhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     * @Route("/user/{id}")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);


        $AddPhotoForm = $this->createForm(addPhotoType::class, null, [
            'action' => $this->generateUrl('app_user_addphoto'),
            'method' => 'POST'
        ]);

        $photos = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->findBy(
                [
                    'author' => $user->getId()
                ]
            );
        $deleteForm = [];
        foreach ($photos as $photo){
            $deleteForm[$photo->getId()] = $this->createForm(deletePhotoType::class, null, [
                'method' => 'DELETE',
                'action' => $this->generateUrl('app_user_deleteownphoto', [
                    'photo_id' => $photo->getId()
                ])
            ])->createView();
        }

        return $this->render('@App/user_page.html.twig', array(
            'user' => $user,
            'photos' => $photos,
            'addForm' => $AddPhotoForm->createView(),
            'deleteForm' => $deleteForm
        ));
    }

    /**
     * @Method("POST")
     * @Route("/new_photo")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPhotoAction(Request $request)
    {
        $photo = new Photo();

        $form = $this->createForm(addPhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $photo->setAuthor($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
        }

        return $this->redirectToRoute('app_user_index', [
            'id' => $this->getUser()->getId()
        ]);
    }

    /**
     * @Method("delete")
     * @Route("/delete/{photo_id}")
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteOwnPhotoAction(int $photo_id)
    {
        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')
            ->find($photo_id);
        $commentaries = $photo->getCommentaries();
        $em = $this->getDoctrine()->getManager();

        foreach ($commentaries as $commentary){
            $em->remove($commentary);
        }
        $em->remove($photo);
        $em->flush();

        return $this->redirectToRoute('app_user_index', array(
            'id' => $this->getUser()->getId()
        ));
    }
}