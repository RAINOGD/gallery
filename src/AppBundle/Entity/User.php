<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commentary", mappedBy="author")
     */
    protected $commentaries;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="author")
     */
    protected $owned_photos;

    public function __construct()
    {
        $this->commentaries = new ArrayCollection();
        $this->owned_photos = new ArrayCollection();
        parent::__construct();
    }

    public function __toString()
    {
        return $this->username ?: '';
    }

    /**
     * Add commentary
     *
     * @param \AppBundle\Entity\Commentary $commentary
     *
     * @return User
     */
    public function addCommentary(\AppBundle\Entity\Commentary $commentary)
    {
        $this->commentaries[] = $commentary;

        return $this;
    }

    /**
     * Remove commentary
     *
     * @param \AppBundle\Entity\Commentary $commentary
     */
    public function removeCommentary(\AppBundle\Entity\Commentary $commentary)
    {
        $this->commentaries->removeElement($commentary);
    }

    /**
     * Get commentaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaries()
    {
        return $this->commentaries;
    }

    /**
     * Add ownedPhoto
     *
     * @param \AppBundle\Entity\Photo $ownedPhoto
     *
     * @return User
     */
    public function addOwnedPhoto(\AppBundle\Entity\Photo $ownedPhoto)
    {
        $this->owned_photos[] = $ownedPhoto;

        return $this;
    }

    /**
     * Remove ownedPhoto
     *
     * @param \AppBundle\Entity\Photo $ownedPhoto
     */
    public function removeOwnedPhoto(\AppBundle\Entity\Photo $ownedPhoto)
    {
        $this->owned_photos->removeElement($ownedPhoto);
    }

    /**
     * Get ownedPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwnedPhotos()
    {
        return $this->owned_photos;
    }
}
