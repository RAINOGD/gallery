<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CommentaryAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('commentary')
            ->add('score');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('commentary')
            ->add('score')
            ->add('author', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('photo', EntityType::class, [
                'class' => 'AppBundle\Entity\Photo'
            ])

            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('commentary')
            ->add('score')
            ->add('author', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('photo', EntityType::class, [
                'class' => 'AppBundle\Entity\Photo'
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('commentary')
            ->add('score')
            ->add('author', EntityType::class, [
                'class' => 'AppBundle\Entity\User'
            ])
            ->add('photo', EntityType::class, [
                'class' => 'AppBundle\Entity\Photo'
            ])
        ;
    }
}
