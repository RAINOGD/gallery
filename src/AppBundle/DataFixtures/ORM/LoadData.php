<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Commentary;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $users = $this->loadUsers();
        $admin = $this->loadAdmin();

        /** @var Photo[] $photos */
        $photos = $this->loadPhotos();
        /** @var Commentary[] $commentaries */
        $commentaries = $this->loadCommentaries();

        $usersArrayLastIndex = count($users) - 1;
        $photosArrayLastIndex = count($photos) - 1;

        foreach ($photos as $photo){
            $photo->setAuthor($users[rand(0, $usersArrayLastIndex)]);
            $manager->persist($photo);
        }

        foreach ($commentaries as $commentary){
            $commentary->setAuthor($users[rand(0, $usersArrayLastIndex)])
                ->setPhoto($photos[rand(0, $photosArrayLastIndex)]);
            $manager->persist($commentary);
        }

        foreach ($users as $user){
            $manager->persist($user);
        }

        $manager->persist($admin);
        $manager->flush();
    }

    public function loadPhotos()
    {
        $photos = [];

        for ($i = 1; $i <= 10; $i++) {
            $photo = new Photo();
            $photo->setName('photo' . $i)
                ->setPhotoName('photo' . $i . '.jpg');

            $photos[] = $photo;
        }

        return $photos;
    }

    public function loadUsers(){
        $users = [];

        for ($i = 0; $i < 5; $i++){
            $user = new User();
            $user->setUsername('character' . $i)
                ->setEmail('character@character' . $i . '.god')
                ->setEnabled(true)
                ->setRoles(['ROLE_USER']);

            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($user, '111');
            $user->setPassword($password);

            $users[] = $user;
        }

        return $users;
    }

    public function loadCommentaries(){


        $commentaries = [];

        for ($i = 1; $i <= 20; $i++){
            $commentary = new Commentary();
            $commentary
                ->setCommentary('Комментарий - ' . $i)
                ->setScore(rand(1, 5));

            $commentaries[] = $commentary;
        }
        return $commentaries;
    }

    public function loadAdmin(){
        $admin = new User();
        $admin->setUsername('AdminGOD')
            ->setEmail('admin@admin.god')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN']);

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($admin, 'admin');
        $admin->setPassword($password);

        return $admin;
    }
}